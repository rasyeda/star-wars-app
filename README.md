# star_wars_application

A Star Wars Application

## Installation

``` 
flutter packages get
flutter run --release
```

This sample project uses Bloc Pattern with a little different approach. I'm using Provider package to provide the business logic instead of using flutter_bloc package.