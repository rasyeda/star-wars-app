import 'dart:collection';

import 'package:star_wars_application/data/models/film.dart';
import 'package:star_wars_application/data/models/film_provider.dart';

class FilmRepository {
  //
  // Singleton film repository
  //
  FilmRepository._();
  static final FilmRepository _repository = FilmRepository._();
  factory FilmRepository() => _repository;

  FilmProvider _filmProvider = FilmProvider();

  HashMap<String, Film> _cachedFilm = HashMap<String, Film>();

  Future<Film> getFilm(String url) async {
    if (!_cachedFilm.containsKey(url)) {
      _cachedFilm[url] = await _filmProvider.getFilm(url);
    }
    return _cachedFilm[url];
  }
}
