import 'dart:collection';
import 'package:star_wars_application/data/models/species.dart';
import 'package:star_wars_application/data/models/species_provider.dart';

class SpeciesRepository {
  //
  // Singleton film repository
  //
  SpeciesRepository._();
  static final SpeciesRepository _repository = SpeciesRepository._();
  factory SpeciesRepository() => _repository;

  SpeciesProvider _filmProvider = SpeciesProvider();

  HashMap<String, Species> _cachedSpecies = HashMap<String, Species>();

  Future<Species> getSpecies(String url) async {
    if (!_cachedSpecies.containsKey(url)) {
      _cachedSpecies[url] = await _filmProvider.getSpecies(url);
    }
    return _cachedSpecies[url];
  }
}
