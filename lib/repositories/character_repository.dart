import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/data/models/character_provider.dart';

class CharacterRepository {
  CharacterProvider _characterProvider = CharacterProvider();

  Future<List<Character>> getCharacters([String nameOrder]) async {
    // Read characters from the database
    List<Character> characters = await _characterProvider.getCharacters(nameOrder);

    // if character db is empty, fetch it from api & save to db
    if (characters.isEmpty) {
      await _characterProvider.fetchCharactertoDb();
      characters = await _characterProvider.getCharacters(nameOrder);
    }
    return characters;
  }

  Future<bool> addCharacter(Character character) =>
      _characterProvider.insert(character).then((value) => character != null);

  Future<bool> editCharacter(Character character) => _characterProvider.update(character);

  Future<bool> deleteCharacter(int id) => _characterProvider.delete(id);
}
