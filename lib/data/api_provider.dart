import 'dart:convert';
import 'package:http/http.dart';

class ApiProvider {
  static final String baseUrl = 'https://swapi.co/';

  // Singleton
  static final ApiProvider _singleton = new ApiProvider._internal();
  factory ApiProvider() => _singleton;
  ApiProvider._internal();

  Future<Map<String, dynamic>> getRequest(String url, [Map<String, String> parameters]) async {
    // Uri uri = Uri.http(_baseUrl, url, parameters);
    Response response = await get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception("Error while fetching data");
    }
  }

}
