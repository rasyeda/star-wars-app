import 'package:sqflite/sqflite.dart';
import 'package:star_wars_application/data/api_provider.dart';
import 'package:star_wars_application/data/db_provider.dart';
import 'package:star_wars_application/data/models/character.dart';


class CharacterProvider {
  final DBProvider _dbProvider = DBProvider();
  final ApiProvider _apiProvider = ApiProvider();
  final String _apiUrl = ApiProvider.baseUrl + 'api/people/';
  final String _tableName = 'characters';

  Future<void> fetchCharactertoDb() async {
    Database db = await _dbProvider.database;

    try {
      Map<String, dynamic> responseBody = await _apiProvider.getRequest(_apiUrl);
      List<Character> characters =
          responseBody['results'].map<Character>((f) => Character.fromJson(f)).toList();
      db.transaction((txn) {
        characters.forEach((char) {
          txn.insert(_tableName, char.toJson());
        });
        return;
      });
    } catch (e) {
      print(e);
    }
  }

  Future<List<Character>> getCharacters(String sortOrder) async {
    Database db = await _dbProvider.database;

    String sortArgs = sortOrder != null ? 'name $sortOrder' : null;
    List<Map<String, dynamic>> results = await db.query(_tableName, orderBy: sortArgs);
    return results.isNotEmpty
        ? results.map((Map<String, dynamic> map) => Character.fromJsonDB(map)).toList()
        : [];
  }

  Future<Character> insert(Character character) async {
    Database db = await _dbProvider.database;
    character.id = await db.insert(_tableName, character.toJson());
    return character;
  }

  Future<bool> update(Character character) async {
    Database db = await _dbProvider.database;
    return await db
            .update(_tableName, character.toJson(), where: 'id = ?', whereArgs: [character.id]) ==
        1;
  }

  Future<bool> delete(int id) async {
    Database db = await _dbProvider.database;
    return await db.delete(_tableName, where: 'id = ?', whereArgs: [id]) == 1;
  }
}
