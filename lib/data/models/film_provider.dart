import 'package:star_wars_application/data/api_provider.dart';
import 'package:star_wars_application/data/models/film.dart';

class FilmProvider {
  final ApiProvider _apiProvider = ApiProvider();

  Future<Film> getFilm(String url) async {
    try {
      Map<String, dynamic> responseBody = await _apiProvider.getRequest(url);
      return Film.fromJson(responseBody);
    } catch (e) {
      print(e);
    }
  }
}
