import 'dart:convert';

class Character {
  int id;
  String name;
  String height;
  String mass;
  String hairColor;
  String skinColor;
  String eyeColor;
  String birthYear;
  String gender;
  String homeworld;
  List<String> films;
  List<String> species;
  List<String> vehicles;
  List<String> starships;
  DateTime created;
  DateTime edited;
  String url;

  Character({
    this.id,
    this.name,
    this.height,
    this.mass,
    this.hairColor,
    this.skinColor,
    this.eyeColor,
    this.birthYear,
    this.gender,
    this.homeworld,
    this.films,
    this.species,
    this.vehicles,
    this.starships,
    this.created,
    this.edited,
    this.url,
  });

  factory Character.empty() => Character(
        films: [],
        species: [],
        vehicles: [],
        starships: [],
        created: DateTime.now(),
        edited: DateTime.now(),
      );

  factory Character.fromJson(Map<String, dynamic> json) => Character(
        id: json['id'] ?? null,
        name: json["name"],
        height: json["height"],
        mass: json["mass"],
        hairColor: json["hair_color"],
        skinColor: json["skin_color"],
        eyeColor: json["eye_color"],
        birthYear: json["birth_year"],
        gender: json["gender"],
        homeworld: json["homeworld"],
        films: List<String>.from(json["films"].map((x) => x)),
        species: List<String>.from(json["species"].map((x) => x)),
        vehicles: List<String>.from(json["vehicles"].map((x) => x)),
        starships: List<String>.from(json["starships"].map((x) => x)),
        created: DateTime.parse(json["created"]),
        edited: DateTime.parse(json["edited"]),
        url: json["url"],
      );

  factory Character.fromJsonDB(Map<String, dynamic> json) => Character(
        id: json['id'] ?? null,
        name: json["name"],
        height: json["height"],
        mass: json["mass"],
        hairColor: json["hair_color"],
        skinColor: json["skin_color"],
        eyeColor: json["eye_color"],
        birthYear: json["birth_year"],
        gender: json["gender"],
        homeworld: json["homeworld"],
        films: List<String>.from(jsonDecode(json["films"]).map((x) => x)),
        species: List<String>.from(jsonDecode(json["species"]).map((x) => x)),
        vehicles: List<String>.from(jsonDecode(json["vehicles"]).map((x) => x)),
        starships: List<String>.from(jsonDecode(json["starships"]).map((x) => x)),
        created: DateTime.parse(json["created"]),
        edited: DateTime.parse(json["edited"]),
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "height": height,
        "mass": mass,
        "hair_color": hairColor,
        "skin_color": skinColor,
        "eye_color": eyeColor,
        "birth_year": birthYear,
        "gender": gender,
        "homeworld": homeworld,
        "films": jsonEncode(List<dynamic>.from(films.map((x) => x))),
        "species": jsonEncode(List<dynamic>.from(species.map((x) => x))),
        "vehicles": jsonEncode(List<dynamic>.from(vehicles.map((x) => x))),
        "starships": jsonEncode(List<dynamic>.from(starships.map((x) => x))),
        "created": created.toIso8601String(),
        "edited": edited.toIso8601String(),
        "url": url,
      };
}
