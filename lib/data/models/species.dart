
class Species {
    String name;
    String classification;
    String designation;
    String averageHeight;
    String skinColors;
    String hairColors;
    String eyeColors;
    String averageLifespan;
    dynamic homeworld;
    String language;

    Species({
        this.name,
        this.classification,
        this.designation,
        this.averageHeight,
        this.skinColors,
        this.hairColors,
        this.eyeColors,
        this.averageLifespan,
        this.homeworld,
        this.language,
    });

    factory Species.fromJson(Map<String, dynamic> json) => Species(
        name: json["name"],
        classification: json["classification"],
        designation: json["designation"],
        averageHeight: json["average_height"],
        skinColors: json["skin_colors"],
        hairColors: json["hair_colors"],
        eyeColors: json["eye_colors"],
        averageLifespan: json["average_lifespan"],
        homeworld: json["homeworld"],
        language: json["language"],
    );

}
