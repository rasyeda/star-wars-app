
class Film {
    String title;
    int episodeId;
    String openingCrawl;
    String director;
    String producer;
    DateTime releaseDate;

    Film({
        this.title,
        this.episodeId,
        this.openingCrawl,
        this.director,
        this.producer,
        this.releaseDate,
    });

    factory Film.fromJson(Map<String, dynamic> json) => Film(
        title: json["title"],
        episodeId: json["episode_id"],
        openingCrawl: json["opening_crawl"],
        director: json["director"],
        producer: json["producer"],
        releaseDate: DateTime.parse(json["release_date"]),
    );
}
