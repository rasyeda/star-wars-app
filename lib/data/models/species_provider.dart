import 'package:star_wars_application/data/api_provider.dart';
import 'package:star_wars_application/data/models/species.dart';

class SpeciesProvider {
  final ApiProvider _apiProvider = ApiProvider();

  Future<Species> getSpecies(String url) async {
    try {
      Map<String, dynamic> responseBody = await _apiProvider.getRequest(url);
      return Species.fromJson(responseBody);
    } catch (e) {
      print(e);
    }
  }
}
