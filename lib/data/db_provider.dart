import 'package:sqflite/sqflite.dart';

class DBProvider {
  static final String _dbFileName = 'star_wars.db';
  Database db;

  // Singleton db provider
  DBProvider._();
  static final DBProvider provider = DBProvider._();
  factory DBProvider() => provider;

  Future<Database> get database async {
    if (db != null) return db;
    // if db is null we instantiate it
    db = await open();
    return db;
  }

  Future open() async {
    String path = await getDatabasesPath() + _dbFileName;
    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        Batch batch = db.batch();
        // Create all tables
        _createTableCharacters(batch);
        await batch.commit();
      },
    );
  }

  
  // CREATE TABLES
  void _createTableCharacters(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS characters');
    batch.execute('''create table characters ( 
        id integer primary key autoincrement, 
        name text,
        height text,
        mass text,
        hair_color text,
        skin_color text,
        eye_color text,
        birth_year text,
        gender text,
        homeworld text,
        films text,
        species text,
        vehicles text,
        starships text,
        created text,
        edited text,
        url text)
    ''');
  }

}
