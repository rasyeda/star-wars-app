import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:star_wars_application/blocs/home_bloc.dart';
import 'package:star_wars_application/ui/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Star Wars App',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        inputDecorationTheme: InputDecorationTheme(
          filled: true,
          border: InputBorder.none,
        ),
        buttonTheme: ButtonThemeData(
          shape: StadiumBorder(),
          buttonColor: Colors.orange,
        )
      ),
      home: Provider<HomeBloc>(
        create: (context) => HomeBloc(),
        dispose: (context, bloc) => bloc.dispose(),
        child: HomePage()),
    );
  }
}
