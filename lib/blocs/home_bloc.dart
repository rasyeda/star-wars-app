import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/repositories/character_repository.dart';

class HomeBloc {
  final CharacterRepository _characterRepository = CharacterRepository();

  HomeBloc() {
    _loadInitialData();
    // reload whenever name order changes
    nameOrderNotifier.addListener(loadCharacters);
  }

  // order by notifier, true for ascending, false for descending, null for unordered
  ValueNotifier<bool> nameOrderNotifier = ValueNotifier<bool>(null);

  // Characters stream
  BehaviorSubject<List<Character>> _charactersController = BehaviorSubject<List<Character>>();
  ValueStream<List<Character>> get characters => _charactersController;

  void _loadInitialData() async {
    loadCharacters();
  }

  void loadCharacters() async {
    String nameOrderArgs;
    if (nameOrderNotifier.value != null) {
      nameOrderArgs = nameOrderNotifier.value ? 'ASC' : 'DESC';
    }
    _charactersController.sink.add(await _characterRepository.getCharacters(nameOrderArgs));
  }

  void dispose() {
    _charactersController?.close();
  }
}
