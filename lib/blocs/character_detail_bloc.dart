import 'package:star_wars_application/blocs/home_bloc.dart';
import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/data/models/film.dart';
import 'package:star_wars_application/data/models/species.dart';
import 'package:star_wars_application/repositories/character_repository.dart';
import 'package:star_wars_application/repositories/film_repository.dart';
import 'package:star_wars_application/repositories/species_repository.dart';

class CharacterDetailBloc {
  final HomeBloc homeBloc;
  final Character character;
  final CharacterRepository _characterRepository = CharacterRepository();
  final FilmRepository _filmRepository = FilmRepository();
  final SpeciesRepository _speciesRepository = SpeciesRepository();

  CharacterDetailBloc(this.homeBloc, this.character);

  void onNameChanged(String value) => character.name = value;
  void onHeightChanged(String value) => character.height = value;
  void onMassChanged(String value) => character.mass = value;
  void onHairColorChanged(String value) => character.hairColor = value;
  void onSkinColorChanged(String value) => character.skinColor = value;
  void onEyeColorChanged(String value) => character.eyeColor = value;
  void onBirthYearChanged(String value) => character.birthYear = value;
  void onGenderChanged(String value) => character.gender = value;

  Future<Film> getFilm(String url) {
    return _filmRepository.getFilm(url);
  }

  Future<Species> getSpecies(String url) {
    return _speciesRepository.getSpecies(url);
  }

  Future<bool> deleteCharacter(int id) {
    return _characterRepository.deleteCharacter(character.id);
  }

  Future<bool> editCharacter() {
    return _characterRepository.editCharacter(character);
  }

  void reloadCharacterList() {
    homeBloc.loadCharacters();
  }

}
