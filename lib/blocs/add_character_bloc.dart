import 'package:star_wars_application/blocs/home_bloc.dart';
import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/repositories/character_repository.dart';

class AddCharacterBloc {
  final HomeBloc homeBloc;
  final Character character = Character.empty();
  final CharacterRepository _characterRepository = CharacterRepository();

  AddCharacterBloc(this.homeBloc);

  void onNameChanged(String value) => character.name = value;
  void onHeightChanged(String value) => character.height = value;
  void onMassChanged(String value) => character.mass = value;
  void onHairColorChanged(String value) => character.hairColor = value;
  void onSkinColorChanged(String value) => character.skinColor = value;
  void onEyeColorChanged(String value) => character.eyeColor = value;
  void onBirthYearChanged(String value) => character.birthYear = value;
  void onGenderChanged(String value) => character.gender = value;


  Future<bool> addCharacter() {
    return _characterRepository.addCharacter(character);
  }

  void reloadCharacterList() {
    homeBloc.loadCharacters();
  }

}
