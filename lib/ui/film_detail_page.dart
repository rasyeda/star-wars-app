import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:star_wars_application/data/models/film.dart';

class FilmDetailPage extends StatelessWidget {
  final Film film;

  const FilmDetailPage({Key key, @required this.film}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(film.title)),
        body: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            InputDecorator(
              decoration: InputDecoration(labelText: 'Title'),
              child: Text(film.title),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Episode ID'),
              child: Text(film.episodeId.toString()),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Opening Crawl'),
              child: Text(film.openingCrawl),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Director'),
              child: Text(film.director),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Producer'),
              child: Text(film.producer),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Release Date'),
              child: Text(DateFormat.yMMMMd().format(film.releaseDate)),
            ),
          ],
        ));
  }
}
