import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:star_wars_application/blocs/character_detail_bloc.dart';
import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/data/models/film.dart';
import 'package:star_wars_application/data/models/species.dart';
import 'package:star_wars_application/ui/film_detail_page.dart';
import 'package:star_wars_application/ui/loading_indicator.dart';
import 'package:star_wars_application/ui/species_detail_page.dart';

class CharacterDetailPage extends StatelessWidget {
  final ValueNotifier<bool> _formEditable = ValueNotifier<bool>(false);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    CharacterDetailBloc bloc = Provider.of<CharacterDetailBloc>(context, listen: false);
    Character character = bloc.character;
    // Form validators
    final requiredValidator = RequiredValidator(errorText: 'This field is required');
    final numberValidator = MultiValidator([
      requiredValidator,
      RangeValidator(min: 1, max: 1000, errorText: 'This field is required'),
    ]);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(character.name),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.edit), onPressed: () => _formEditable.value = true),
          IconButton(
              icon: Icon(
                Icons.delete,
              ),
              onPressed: () async {
                bool deleteConfirmed = await showDialog<bool>(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) => _deleteConfirmationDialog(context),
                );
                if (deleteConfirmed) {
                  if (await bloc.deleteCharacter(character.id)) {
                    // when the character deleted, also refresh character list in home page
                    bloc.reloadCharacterList();

                    final state = _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text('Success! Character has been deleted.'),
                    ));

                    // pop this page immediately after snackbar closed
                    state.closed.whenComplete(() => Navigator.of(context).pop());
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(
                          'Failed! There is something wrong, please try again in a few moments.'),
                    ));
                  }
                }
              }),
        ],
      ),
      body: Form(
        key: _formKey,
        child: ValueListenableBuilder<bool>(
          valueListenable: _formEditable,
          builder: (context, formEditable, _) => ListView(
            padding: EdgeInsets.all(16.0),
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Name'),
                initialValue: character.name,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onNameChanged,
                validator: requiredValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Height'),
                initialValue: character.height,
                keyboardType: TextInputType.number,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onHeightChanged,
                validator: numberValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Mass'),
                initialValue: character.mass,
                keyboardType: TextInputType.number,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onMassChanged,
                validator: numberValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Hair Color'),
                initialValue: character.hairColor,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onHairColorChanged,
                validator: requiredValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Skin Color'),
                initialValue: character.skinColor,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onSkinColorChanged,
                validator: requiredValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Eye Color'),
                initialValue: character.eyeColor,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onEyeColorChanged,
                validator: requiredValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Birth Year'),
                initialValue: character.birthYear,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onBirthYearChanged,
                validator: requiredValidator,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Gender'),
                initialValue: character.gender,
                readOnly: !formEditable,
                enabled: formEditable,
                onChanged: bloc.onGenderChanged,
                validator: requiredValidator,
              ),
              Visibility(
                  visible: formEditable,
                  child: RaisedButton.icon(
                    onPressed: () async {
                      // validate first
                      if (_formKey.currentState.validate()) {

                        // show dialog
                        bool editConfirmed = await showDialog<bool>(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) => _editConfirmationDialog(context),
                        );
                        if (editConfirmed) {
                          if (await bloc.editCharacter()) {
                            // when the character updated, also refresh character list in home page
                            bloc.reloadCharacterList();

                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text('Success! Character has been saved.'),
                            ));
                          } else {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text(
                                  'Failed! There is something wrong, please try again in a few moments.'),
                            ));
                          }
                        }
                      }
                    },
                    icon: Icon(Icons.edit),
                    label: Text('Submit'),
                    textColor: Colors.white,
                  )),
              ExpansionTile(
                  title: Text('Films'),
                  initiallyExpanded: true,
                  children: character.films
                      .map((e) => FutureBuilder<Film>(
                          future: bloc.getFilm(e),
                          builder: (context, snapshot) => !snapshot.hasData
                              ? Center(child: LoadingIndicator())
                              : ListTile(
                                  onTap: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => FilmDetailPage(
                                              film: snapshot.data,
                                            )));
                                  },
                                  title: Text(snapshot.data.title),
                                  subtitle: Text('Released ' +
                                      DateFormat.yMMMd().format(snapshot.data.releaseDate)),
                                )))
                      .toList()),
              ExpansionTile(
                  title: Text('Species'),
                  initiallyExpanded: true,
                  children: character.species
                      .map((e) => FutureBuilder<Species>(
                          future: bloc.getSpecies(e),
                          builder: (context, snapshot) => !snapshot.hasData
                              ? Center(child: LoadingIndicator())
                              : ListTile(
                                  onTap: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => SpeciesDetailpage(
                                              species: snapshot.data,
                                            )));
                                  },
                                  title: Text(snapshot.data.name),
                                  subtitle:
                                      Text('Classification : ' + snapshot.data.classification),
                                )))
                      .toList()),
            ]
                .map((e) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 4.0),
                      child: e,
                    ))
                .toList(),
          ),
        ),
      ),
    );
  }

  Widget _editConfirmationDialog(BuildContext context) => AlertDialog(
        title: Text('Edit Confirmation'),
        content: Text('Are you sure want to edit this character?'),
        actions: <Widget>[
          FlatButton(onPressed: () => Navigator.of(context).pop(false), child: Text('Cancel')),
          RaisedButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Confirm'),
              textColor: Colors.white),
        ],
      );

  Widget _deleteConfirmationDialog(BuildContext context) => AlertDialog(
        title: Text('Delete Confirmation'),
        content: Text('Are you sure want to delete this character?'),
        actions: <Widget>[
          FlatButton(onPressed: () => Navigator.of(context).pop(false), child: Text('Cancel')),
          RaisedButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Confirm'),
              textColor: Colors.white),
        ],
      );
}
