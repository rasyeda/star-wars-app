import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:star_wars_application/blocs/add_character_bloc.dart';
import 'package:star_wars_application/blocs/character_detail_bloc.dart';
import 'package:star_wars_application/blocs/home_bloc.dart';
import 'package:star_wars_application/data/models/character.dart';
import 'package:star_wars_application/ui/add_character_page.dart';
import 'package:star_wars_application/ui/character_detail_page.dart';
import 'package:star_wars_application/ui/loading_indicator.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    HomeBloc bloc = Provider.of<HomeBloc>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Star Wars App'),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: ListView(
        padding: EdgeInsets.all(12.0),
        children: <Widget>[
          ListTile(
            title: Center(
                child: Text(
              'Star Wars Characters',
              style: Theme.of(context).textTheme.headline6,
            )),
            trailing: ValueListenableBuilder<bool>(
                valueListenable: bloc.nameOrderNotifier,
                builder: (context, nameOrder, _) {
                  return IconButton(
                      icon: Icon(Icons.sort_by_alpha),
                      onPressed: () {
                        bloc.nameOrderNotifier.value = nameOrder == null ? true : !nameOrder;
                      });
                }),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Divider(),
          ),
          StreamBuilder<List<Character>>(
              stream: bloc.characters,
              builder: (context, snapshot) {
                return !snapshot.hasData
                    ? Center(child: LoadingIndicator())
                    : Column(
                        children: snapshot.data
                            .map(
                              (character) => Card(
                                child: ListTile(
                                  title: Text(character.name),
                                  onTap: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => Provider<CharacterDetailBloc>(
                                            create: (context) =>
                                                CharacterDetailBloc(bloc, character),
                                            child: CharacterDetailPage())));
                                  },
                                ),
                              ),
                            )
                            .toList());
              }),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => Provider<AddCharacterBloc>(
                    create: (context) => AddCharacterBloc(bloc),
                    child: AddCharacterPage(),
                  )));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
