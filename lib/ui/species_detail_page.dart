import 'package:flutter/material.dart';
import 'package:star_wars_application/data/models/species.dart';

class SpeciesDetailpage extends StatelessWidget {
  final Species species;

  const SpeciesDetailpage({Key key, @required this.species}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        appBar: AppBar(title: Text(species.name)),
        body: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            InputDecorator(
              decoration: InputDecoration(labelText: 'Title'),
              child: Text(species.name),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Classification'),
              child: Text(species.classification.toString()),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Designation'),
              child: Text(species.designation),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Average Height'),
              child: Text(species.averageHeight),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Skin Colors'),
              child: Text(species.skinColors),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Hair Colors'),
              child: Text(species.hairColors),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Eye Colors'),
              child: Text(species.eyeColors),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Average Lifespan'),
              child: Text(species.averageLifespan),
            ),
            InputDecorator(
              decoration: InputDecoration(labelText: 'Language'),
              child: Text(species.language),
            ),
          ],
        )
    );
  }
}