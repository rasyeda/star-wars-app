import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';
import 'package:star_wars_application/blocs/add_character_bloc.dart';

class AddCharacterPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    AddCharacterBloc bloc = Provider.of<AddCharacterBloc>(context, listen: false);
    
    // Form validators
    final requiredValidator = RequiredValidator(errorText: 'This field is required');
    final numberValidator = MultiValidator([
      requiredValidator,
      RangeValidator(min: 1, max: 1000, errorText: 'This field is required'),
    ]);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Add new character'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: 'Name'),
              onChanged: bloc.onNameChanged,
              validator: requiredValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Height'),
              keyboardType: TextInputType.number,
              onChanged: bloc.onHeightChanged,
              validator: numberValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Mass'),
              keyboardType: TextInputType.number,
              onChanged: bloc.onMassChanged,
              validator: numberValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Hair Color'),
              onChanged: bloc.onHairColorChanged,
              validator: requiredValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Skin Color'),
              onChanged: bloc.onSkinColorChanged,
              validator: requiredValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Eye Color'),
              onChanged: bloc.onEyeColorChanged,
              validator: requiredValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Birth Year'),
              onChanged: bloc.onBirthYearChanged,
              validator: requiredValidator,
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Gender'),
              onChanged: bloc.onGenderChanged,
              validator: requiredValidator,
            ),
            RaisedButton.icon(
              onPressed: () async {
                // validate the form
                if (_formKey.currentState.validate()) {
                  // save the character
                  if (await bloc.addCharacter()) {
                    // when the character updated, also refresh character list in home page
                    bloc.reloadCharacterList();

                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text('Success! Character has been saved.'),
                    ));
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(
                          'Failed! There is something wrong, please try again in a few moments.'),
                    ));
                  }
                }
              },
              icon: Icon(Icons.add),
              label: Text('Submit'),
              textColor: Colors.white,
            ),
          ]
              .map((e) => Padding(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    child: e,
                  ))
              .toList(),
        ),
      ),
    );
  }
}
